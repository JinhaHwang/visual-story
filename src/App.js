import React from 'react';
import {hot} from 'react-hot-loader/root';
import './App.scss';

function App() {
  return (
    <div className="app">
      <div className="left">

      </div>
      <div className="right">
          <div className="contents">
              <div className="contents-top">
                  <div className="contents-top-tree" />
                  <div className="contents-top-form">
                      <input className="contents-top-form-input" style={{ width: 400 }}/>
                  </div>
              </div>
              <div className="contents-bottom" />
              <div className="block" />
          </div>
      </div>
    </div>
  );
}

export default hot(App);
