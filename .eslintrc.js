module.exports = {
    extends: ['react-app', 'airbnb', 'prettier', 'prettier/react'],
    parser: 'babel-eslint',
    rules: {
        // 'no-console': 'off',
        // 'no-undef': 'off',
        // 'no-unused-vars': 'off',
        // 'react/prop-types': 'off',

        'react/prefer-stateless-function': 0,
        'react/jsx-filename-extension': 0,
        'react/jsx-one-expression-per-line': 0,
    },
    env: {
        browser: true,
        es6: true,
        node: true,
    },
}
